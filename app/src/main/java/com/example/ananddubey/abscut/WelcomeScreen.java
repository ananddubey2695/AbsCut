package com.example.ananddubey.abscut;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

public class WelcomeScreen extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>21) {

            Slide slide = new Slide();

            getWindow().setExitTransition(slide);
        }
        setContentView(R.layout.activity_welcome_screen);
        ImageView im=(ImageView)findViewById(R.id.imageView_wellogo);
        ImageView im1=(ImageView)findViewById(R.id.ImageView_logo);
        TextView tx=(TextView)findViewById(R.id.textView_logo_text);
        TextView tx1=(TextView)findViewById(R.id.textView_fi);
        TextView tx2=(TextView)findViewById(R.id.textView_smile);

        OvershootInterpolator oi=new OvershootInterpolator(10);
        DecelerateInterpolator di=new DecelerateInterpolator();
        BounceInterpolator bi=new BounceInterpolator();



        Animation anim= AnimationUtils.loadAnimation(this, R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.slide_in_left);
        Animation anim2=AnimationUtils.loadAnimation(this,R.anim.slide_in_left);
        Animation anim3=AnimationUtils.loadAnimation(this,R.anim.slide_in_up);
        Animation anim4=AnimationUtils.loadAnimation(this,R.anim.slide_in_up);
        anim1.setInterpolator(oi);
        anim.setInterpolator(di);

        anim3.setInterpolator(bi);
        anim1.setStartOffset(2500);
        anim2.setStartOffset(3000);
        anim3.setStartOffset(4000);

        im.startAnimation(anim);
        im1.startAnimation(anim4);
        tx.startAnimation(anim1);
        tx1.startAnimation(anim2);
        tx2.startAnimation(anim3);

        anim3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent i=new Intent(WelcomeScreen.this,MainActivity.class);
                ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation(WelcomeScreen.this,null);


                startActivity(i,compat.toBundle());
                finish();


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });












    }
}
