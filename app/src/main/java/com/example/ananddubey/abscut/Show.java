package com.example.ananddubey.abscut;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.widget.ImageView;
import android.widget.TextView;

public class Show extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {
            getWindow().setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.pic_trans));
            /*Slide slide=new Slide();
            slide.setDuration(500);
            Explode explode=new Explode();
            explode.setDuration(500);
            getWindow().setEnterTransition(slide);

            getWindow().setReturnTransition(explode);
            */

        }
        setContentView(R.layout.activity_show);
        TextView name=(TextView)findViewById(R.id.textView3);
        TextView intensity=(TextView)findViewById(R.id.textView4);
        ImageView img=(ImageView)findViewById(R.id.imageView3);


        name.setText(getIntent().getStringExtra("ex_name"));
        intensity.setText(getIntent().getStringExtra("ex_intensity"));
        img.setImageResource(getIntent().getIntExtra("img_id", 00));
    }
}
