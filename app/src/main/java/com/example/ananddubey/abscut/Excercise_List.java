package com.example.ananddubey.abscut;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.Explode;

import android.transition.SidePropagation;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class Excercise_List extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    String[] E_name,I_name;

    int[] Img_res = {R.drawable.cross,R.drawable.hand_raise,R.drawable.leg_crunches,R.drawable.leg_raise,
            R.drawable.plank,R.drawable.side_bend,R.drawable.simple_crunch,R.drawable.twist,R.drawable.raise_plank,R.drawable.cross_cycle};
    ArrayList<work> arrayList=new ArrayList<work>();




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {

            getWindow().setSharedElementExitTransition(TransitionInflater.from(this).inflateTransition(R.transition.pic_trans));

           /* Explode explode=new Explode();
            explode.setDuration(500);
            Slide slide=new Slide();
            slide.setDuration(500);

            getWindow().setEnterTransition(slide);
            getWindow().setReenterTransition(slide);
            getWindow().setExitTransition(explode);
            getWindow().setReturnTransition(explode);
            */

        }

        setContentView(R.layout.activity_excercise__list);

        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        E_name=getResources().getStringArray(R.array.Excercise_Name);
        I_name=getResources().getStringArray(R.array.Excercise_Intensity);
        int i=0;

        for(String name:E_name)
        {
            work w=new work(Img_res[i],name,I_name[i]);
            arrayList.add(w);
            i++;
        }
        adapter=new RecyclerAdapter(arrayList,this,"y");
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gm);




    }


}



class work
{
    private int iconId;
    private String name;
    private String intensity;


    public work(int iconId, String name, String intensity) {
        this.iconId = iconId;
        this.name = name;
        this.intensity = intensity;
    }

    public int getIconId() {
        return iconId;
    }

    public String getIntensity() {
        return intensity;
    }

    public String getName() {
        return name;
    }
}


class  RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>{
    String axis="y";
    int prevposi=0;


    private ArrayList<work> arrayList=new ArrayList<work>();
    Context ctx;

    public RecyclerAdapter(ArrayList<work>arrayList,Context ctx,String axis)
    {

        this.axis=axis;
        this.arrayList=arrayList;
        this.ctx=ctx;

    }




    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        RecyclerViewHolder recyclerViewHolder=new RecyclerViewHolder(view,ctx,arrayList);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        work w=arrayList.get(position);
        holder.icon.setImageResource(w.getIconId());
        holder.Excercise.setText(w.getName());
        holder.Intensity.setText(w.getIntensity());


            if (position > prevposi) {
                AnimUtils.animate(holder, true);
            } else {
                AnimUtils.animate(holder, false);
            }
            prevposi = position;





    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView Excercise,Intensity;
        ImageView icon;
        ArrayList<work> arrayList=new ArrayList<work>();
        Context ctx;

        public RecyclerViewHolder(View itemView,Context ctx,ArrayList<work> arrayList) {
            super(itemView);
            this.arrayList=arrayList;
            this.ctx=ctx;
            itemView.setOnClickListener(this);
            Excercise = (TextView)itemView.findViewById(R.id.item_TextView_ExcerciseName);
            Intensity = (TextView)itemView.findViewById(R.id.item_textView_Intensity);
            icon = (ImageView)itemView.findViewById(R.id.imageView);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            View w=v.findViewById(icon.getId());
            w.setTransitionName("imgClicked");
            int position =getAdapterPosition();
            work work=this.arrayList.get(position);
            ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)this.ctx,w,w.getTransitionName());
            Intent i=new Intent(this.ctx,Show.class);
            i.putExtra("img_id",work.getIconId());
            i.putExtra("ex_name",work.getName());
            i.putExtra("ex_intensity", work.getIntensity());
            this.ctx.startActivity(i,compat.toBundle());


        }
    }
}

