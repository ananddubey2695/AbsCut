package com.example.ananddubey.abscut;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Routine_Start extends AppCompatActivity {
    MediaPlayer stopaudio1;
    TextView display,display1;
    Button start;
    Button stop;
    Button pause;
    Button resume;
    public int lap;
    public int tim=5000;
    Boolean t=true,abort=false;
    String[] E_name;
    int ex_pos=0;
    Thread t1=new Thread();
    TextView current_Excercise,upcoming_Excercise;
    ImageView img;
    int[] Img_res = new int[10];
    int[] Img_res1 = {R.drawable.simple_crunch,R.drawable.cross,R.drawable.side_bend,R.drawable.plank};
    int[] Img_res2 = {R.drawable.simple_crunch,R.drawable.cross_cycle,R.drawable.leg_raise,R.drawable.plank};
    int[] Img_res3 = {R.drawable.simple_crunch,R.drawable.twist,R.drawable.leg_raise,R.drawable.plank,R.drawable.leg_crunches};
    int[] Img_res4 = {R.drawable.raise_plank,R.drawable.dumbell,R.drawable.simple_crunch,R.drawable.plank};
    int sec=5;









    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    naya n=new naya();

    public Handler han=new Handler()
    {
        public void handleMessage(Message msg) {
            String a = String.valueOf(msg.arg1);

            if(sec==1 || sec==0) {

                pause.setEnabled(false);


            }


            if(a.equals("1212"))
            {
                start.setEnabled(true);
                resume.setEnabled(false);
                pause.setEnabled(false);
            }

            else if(a.equals("444"))
            {
                pause.setEnabled(true);

                if(ex_pos<E_name.length) {
                    display1.setText("5");

                    img.setImageResource(Img_res[ex_pos]);

                    current_Excercise.setText(E_name[ex_pos]);
                }
                else {
                    current_Excercise.setText("Done");
                    display.setText("--");
                    display1.setText("--");
                }
                if(ex_pos<E_name.length-1)
                upcoming_Excercise.setText(E_name[ex_pos+1]);
                else upcoming_Excercise.setText("---");
            }


            else if(a.equals("0"))
            {
                sec--;
                if(sec>=0) {
                    display1.setText(String.valueOf(sec));
                    display.setText(a);
                }
            }
            else
            display.setText(a);
        }
    };







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {

            Explode explode=new Explode();
            explode.setDuration(500);
            Slide slide=new Slide();
            slide.setDuration(500);

            getWindow().setEnterTransition(slide);
            getWindow().setReenterTransition(slide);
            getWindow().setExitTransition(explode);
            getWindow().setReturnTransition(explode);


        }
        setContentView(R.layout.activity_routine__start);

        display1 = (TextView) findViewById(R.id.textView_Routine_Start_Timer);
        display = (TextView) findViewById(R.id.textView_Routine_Start_Timer1);
        start = (Button) findViewById(R.id.button_Routine_Start_Start);
        pause = (Button) findViewById(R.id.button_Routine_Start_Pause);
        resume=(Button)findViewById(R.id.button_Routine_Start_Resume);
        current_Excercise=(TextView)findViewById(R.id.textView_Routine_Start_CurrentExercise);
        upcoming_Excercise=(TextView)findViewById(R.id.textView_Routine_Start_NextExcercise);
        img=(ImageView)findViewById(R.id.imageView_Routine_Start_ShowImage);
        final MediaPlayer startaudio1= MediaPlayer.create(this,R.raw.start);
        final MediaPlayer stopaudio1= MediaPlayer.create(this,R.raw.stop);




        pause.setEnabled(false);
        resume.setEnabled(false);
        start.setEnabled(true);
        String decide=getIntent().getStringExtra("curr_excer");






        switch (decide)
        {
            case "0":
                E_name=getResources().getStringArray(R.array.Level_1);
                Img_res=Img_res1;
                lap=E_name.length;
                break;
            case "1":
                E_name=getResources().getStringArray(R.array.Level_2);
                Img_res=Img_res2;
                lap=E_name.length;
                break;
            case "2":
                E_name=getResources().getStringArray(R.array.Level_3);
                Img_res=Img_res3;
                lap=E_name.length;
                break;
            case "3":
                E_name=getResources().getStringArray(R.array.Level_1);
                Img_res=Img_res4;
                lap=E_name.length;
                break;

        }
        current_Excercise.setText(E_name[0]);
        upcoming_Excercise.setText(E_name[1]);
        img.setImageResource(Img_res[0]);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_Excercise.setText(E_name[0]);
                upcoming_Excercise.setText(E_name[1]);
                ex_pos=0;
                Start(startaudio1,stopaudio1);

            }
        });




    }
    public void Start(final MediaPlayer mp,final MediaPlayer mp1)
    {
        start.setEnabled(false);
        resume.setEnabled(false);
        pause.setEnabled(true);
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {


                for (int j = 1; j <= lap; j++) {
                    if(!abort) {
                        sec=5;
                        mp.start();


                        while (sec > 0)
                        {
                            for (int i = 99; i >= 0; i--) {
                                if (!abort) {


                                    Message msg1 = Message.obtain();
                                    System.out.println("------------------------" + i);
                                    msg1.arg1 = i;
                                    han.sendMessage(msg1);
                                    try {
                                        Thread.sleep(10);

                                    } catch (InterruptedException e) {
                                        synchronized (n) {
                                            try {
                                                n.wait();
                                            } catch (InterruptedException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                    }


                        ex_pos++;
                        if (!abort) {
                            mp1.start();

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Message ms1 = Message.obtain();
                            ms1.arg1 = 444;
                            han.sendMessage(ms1);
                        }
                    }

                }

                Message ms=Message.obtain();
                ms.arg1=1212;
                han.sendMessage(ms);
            }
        });

        t1.start();

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause.setEnabled(false);
                resume.setEnabled(true);
                t1.interrupt();
            }
        });


        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resume.setEnabled(false);
                pause.setEnabled(true);

                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (n) {
                            n.notify();
                        }

                    }
                });
                t2.start();
            }
        });
    }
    protected void onDestroy() {
        abort=true;
        if(t1.isAlive())
        t1.interrupt();
        super.onDestroy();

    }
    protected void onUserLeaveHint() {


        if(t1.isAlive()) {
            t1.interrupt();
        }
        pause.setEnabled(false);
        resume.setEnabled(true);

        super.onUserLeaveHint();
    }




}

