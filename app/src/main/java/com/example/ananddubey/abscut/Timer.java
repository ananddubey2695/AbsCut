package com.example.ananddubey.abscut;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Timer extends AppCompatActivity {
    public TextView display;
    EditText lapE;
    EditText timeE;
    Button start;
    Button stop;
    Button pause;
    Button resume;
    public int lap;
    public int tim;
    Boolean t=true;
    Boolean abort=false;
    Thread t1=new Thread();

    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    naya n=new naya();


    public Handler han=new Handler()
    {
        public void handleMessage(Message msg) {
            String a = String.valueOf(msg.arg1);

            if(a.equals("1212"))
            {
                start.setEnabled(true);
                resume.setEnabled(false);
                pause.setEnabled(false);
            }
            else if(a.equals("444"))
            {
                pause.setEnabled(true);
            }
            else if(a.equals("1")||a.equals("0")) {
                pause.setEnabled(false);
                display.setText(a);
            }
            else display.setText(a);
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        display = (TextView) findViewById(R.id.textView_Display);
        start = (Button) findViewById(R.id.button_Start);
        pause = (Button) findViewById(R.id.button_Pause);
        resume=(Button)findViewById(R.id.button_Resume);
        lapE=(EditText)findViewById(R.id.editText_no_Lap);
        timeE=(EditText)findViewById(R.id.editText_LapSec);
        pause.setEnabled(false);
        resume.setEnabled(false);
        pause.setEnabled(false);
        resume.setEnabled(false);
        start.setEnabled(true);



        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = String.valueOf(lapE.getText());
                String b = String.valueOf(timeE.getText());
                try {
                    tim = Integer.parseInt(b);
                    lap = Integer.parseInt(a);
                    Start();
                } catch (Exception e) {
                    show("Enter Correct Information");
                }
            }
        });
    }


    public void Start()
    {
        start.setEnabled(false);
        resume.setEnabled(false);
        pause.setEnabled(true);
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {


                for (int j = 1; j <= lap; j++) {
                    if(!abort) {

                        for (int i = tim; i >= 0; i--) {
                            if(!abort)
                            {
                            Message msg1 = Message.obtain();
                            System.out.println("------------------------" + i);
                            msg1.arg1 = i;
                            han.sendMessage(msg1);
                            try {
                                Thread.sleep(1000);

                            } catch (InterruptedException e) {
                                synchronized (n) {
                                    try {
                                        n.wait();
                                    } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                        if(!abort) {
                            toneGen1.startTone(ToneGenerator.TONE_SUP_ERROR, 4000);
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            Message ms1 = Message.obtain();
                            ms1.arg1 = 444;
                            han.sendMessage(ms1);
                        }
                }

            }
                Message ms = Message.obtain();
                ms.arg1 = 1212;
                han.sendMessage(ms);

            }
        });

        t1.start();


        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause.setEnabled(false);
                resume.setEnabled(true);
                t1.interrupt();
            }
        });


        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abort=false;

                resume.setEnabled(false);
                pause.setEnabled(true);

                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (n) {
                            n.notify();
                        }

                    }
                });
                t2.start();
            }
        });
    }



    @Override
    protected void onDestroy() {
        System.out.println("IN DESTROY");
        abort=true;

        if(t1.isAlive()) {
            t1.interrupt();
        }
        super.onDestroy();

    }
    @Override
    protected void onUserLeaveHint() {
        System.out.println("IN HOME");
        abort=true;


        if(t1.isAlive()) {
            t1.interrupt();
        }
        pause.setEnabled(false);
        resume.setEnabled(true);

        super.onUserLeaveHint();
    }

    public void show(String a) {

        Toast.makeText(this, a, Toast.LENGTH_SHORT).show();
    }

    public void doit()
    {
        start.setEnabled(false);
        resume.setEnabled(false);
        pause.setEnabled(true);
    }









}


class naya {
    public int get(int max) throws InterruptedException {
        int i=max;
        i--;
        return i;
    }
    public void set() throws InterruptedException {
        synchronized(this)
        {
            System.out.println(" Resume Button PRESSED");
            synchronized (this) {
                notify();
            }
        }
    }
}