package com.example.ananddubey.abscut;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Routines extends AppCompatActivity implements RecyclerAdapter1.ClickListener{
    RecyclerView recyclerView;
    RecyclerView recyclerView1;
    RecyclerView.Adapter adapter,adapter1;
    RecyclerView.LayoutManager layoutManager;

    String[] title,E_name,I_name;
    int[] Img_res = {R.drawable.lvl1,R.drawable.lvl2,R.drawable.lvl3,R.drawable.home};
    int[] Img_res1 = {R.drawable.simple_crunch,R.drawable.cross,R.drawable.side_bend,R.drawable.plank};
    int[] Img_res2 = {R.drawable.simple_crunch,R.drawable.cross_cycle,R.drawable.leg_raise,R.drawable.plank};
    int[] Img_res3 = {R.drawable.simple_crunch,R.drawable.twist,R.drawable.leg_raise,R.drawable.plank,R.drawable.leg_crunches};
    int[] Img_res4 = {R.drawable.plank,R.drawable.cross,R.drawable.simple_crunch,R.drawable.plank};
    ArrayList<routineMenu> arrayList=new ArrayList<routineMenu>();
    ArrayList<work> arrayList1=new ArrayList<work>();
    TextView t1;
    Button routine_Start;
    String  currentExercisePosition="null";
    TextView Discription;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {

            Explode explode=new Explode();
            explode.setDuration(500);
            Slide slide=new Slide();
            slide.setDuration(500);

            getWindow().setEnterTransition(slide);
            getWindow().setReenterTransition(slide);
            getWindow().setExitTransition(explode);
            getWindow().setReturnTransition(explode);


        }
        setContentView(R.layout.activity_routines);
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view_Routine1);


        title=getResources().getStringArray(R.array.Routines_String_Array);
        routine_Start=(Button)findViewById(R.id.button_Routine_Start);
        Discription=(TextView)findViewById(R.id.textView_Routine_Discription);
        Discription.setText("A process paragraph typically begins with a topic sentence, which clearly labels the process and explains its relevance to readers. Any materials needed are listed, followed by the stages of the process; most of the time, this is in chronological order. Each step is typically numbered and includes examples and elaboration with specific details. This is particularly important for task-oriented process paragraphs so that a reader can duplicate the process.\n" +
                "Process paragraphs should provide the rationale for each step and provide warnings when necessary. Terms that might be unfamiliar should also be explained. A task-oriented process paragraph should end with a way for readers to know if they have successfully duplicated the process.\n" +
                "Editing is important when writing a process paragraph; some writers may skip a step that seems too obvious to need explaining. One way to test a process paragraph is to see if someone unfamiliar with the process is able to do it using the paragraph.");







        routine_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentExercisePosition.equals("null"))
                {
                    Toast.makeText(Routines.this, "Please Select One Routine", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent i = new Intent("com.example.ananddubey.abscut.Routine_Start");

                    i.putExtra("curr_excer", currentExercisePosition);

                    ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation(Routines.this,null);
                    startActivity(i,compat.toBundle());
                }


            }
        });

        int i=0;

        for(String name:title)
        {
            routineMenu w=new routineMenu(Img_res[i],name);
            arrayList.add(w);
            i++;
        }

        adapter=new RecyclerAdapter1(arrayList,this,this);




        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);

        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(gm);
    }


    @Override
    public void itemClicked(View v, int position) {

        currentExercisePosition = String.valueOf(position);


        t1=(TextView)findViewById(R.id.textView_Routine_ShowTitle);
        t1.setText(title[position]);
        //String currentExcer = title[position];
        recyclerView1=(RecyclerView)findViewById(R.id.recycler_view_Routine2);

        switch (position)
        {
            case 0:
                Img_res=Img_res1;
                E_name=getResources().getStringArray(R.array.Level_1);
                I_name=getResources().getStringArray(R.array.Excercise_Intensity);
                break;
            case 1:
                Img_res=Img_res2;
                E_name=getResources().getStringArray(R.array.Level_2);
                I_name=getResources().getStringArray(R.array.Excercise_Intensity);
                break;
            case 2:
                Img_res=Img_res3;
                E_name=getResources().getStringArray(R.array.Level_3);
                I_name=getResources().getStringArray(R.array.Excercise_Intensity);
                break;
            case 3:
                Img_res=Img_res4;
                E_name=getResources().getStringArray(R.array.Level_1);
                I_name=getResources().getStringArray(R.array.Excercise_Intensity);
                break;


        }

        int i=0;
        arrayList1.clear();

        for(String name:E_name)
        {
            work w=new work(Img_res[i],name,I_name[i]);
            arrayList1.add(w);
            i++;
        }
        adapter1=new RecyclerAdapter(arrayList1,this,"x");
        recyclerView1.setHasFixedSize(true);

        recyclerView1.setAdapter(adapter1);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL);
        recyclerView1.setLayoutManager(gm);



    }
}



class  RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder1>
{
    private ArrayList<routineMenu> arrayList=new ArrayList<routineMenu>();
    Context ctx;
    private static ClickListener clickListener;


    public RecyclerAdapter1(ArrayList<routineMenu>arrayList,Context ctx,ClickListener clickListener)
    {
        this.arrayList=arrayList;
        this.ctx=ctx;
        this.clickListener=clickListener;

    }


    @Override
    public RecyclerAdapter1.RecyclerViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_routine_menu, parent, false);
        RecyclerViewHolder1 recyclerViewHolder=new RecyclerViewHolder1(view,ctx,arrayList);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter1.RecyclerViewHolder1 holder, int position) {
        routineMenu r=arrayList.get(position);
        holder.icon.setImageResource(r.getIconId());
        holder.Excercise.setText(r.getName());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class RecyclerViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView t11;
        TextView Excercise;
        ImageView icon;
        ArrayList<routineMenu> arrayList=new ArrayList<routineMenu>();
        Context ctx;

        public RecyclerViewHolder1(View itemView,Context ctx,ArrayList<routineMenu> arrayList) {
            super(itemView);
            this.arrayList=arrayList;
            this.ctx=ctx;
            itemView.setOnClickListener(this);
            Excercise = (TextView)itemView.findViewById(R.id.textView_layout_routine_menuTitle);
            icon = (ImageView)itemView.findViewById(R.id.imageView_layout_routine_menuTitle);
        }

        @Override
        public void onClick(View v) {
            int position =getAdapterPosition();
            routineMenu work=this.arrayList.get(position);
            Intent i=new Intent(this.ctx,Excercise_List.class);
            i.putExtra("img_id", work.getIconId());
            i.putExtra("ex_name", work.getName());

            if(clickListener!=null)
            {
                clickListener.itemClicked(v,getAdapterPosition());

            }

        }
    }
    public interface ClickListener
    {
        public void itemClicked(View v,int position);

    }
}




class routineMenu extends Routines
{
    private int iconId;
    private String title;



    public routineMenu(int iconId, String name) {
        this.iconId = iconId;
        this.title = name;

    }

    public int getIconId() {
        return iconId;
    }



    public String getName() {
        return title;
    }
}


