package com.example.ananddubey.abscut;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
ImageButton button_list1,button_counter,button_routines;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {
            Slide slide=new Slide();
            slide.setDuration(500);
            Explode explode=new Explode();
            explode.setDuration(500);
            getWindow().setEnterTransition(explode);
            getWindow().setReturnTransition(explode);
            getWindow().setExitTransition(explode);
            getWindow().setReenterTransition(slide);

        }
        setContentView(R.layout.activity_main);
        button_list1=(ImageButton)findViewById(R.id.imageButton_list);
        button_routines=(ImageButton)findViewById(R.id.imageButton_routine);
        button_counter=(ImageButton)findViewById(R.id.imageButton_timer1);
        View v1=(View)findViewById(R.id.mainLay);

        button_list1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button_list1.setImageResource(R.drawable.timer2);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    button_list1.setImageResource(R.drawable.exercise_list1);
                }

                return false;
            }
        });
        button_list1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent("com.example.ananddubey.abscut.Excercise_List");
                ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,null);
                startActivity(i,compat.toBundle());

            }

        });

        button_routines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent("com.example.ananddubey.abscut.Routines");
                ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,null);
                startActivity(i,compat.toBundle());
            }
        });

        button_counter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent("com.example.ananddubey.abscut.Timer");
                ActivityOptionsCompat compat=ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,null);
                startActivity(i,compat.toBundle());

            }
        });

        button_list1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setBackgroundColor(66698);
                return false;
            }
        });




    }

}
